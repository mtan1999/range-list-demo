# Range List Demo
This a demo project.

RangeList class provides`add`, `remove`, and `print` methods to operate a range list which based on a linked list data structure.

The Time Complexities of all these three methods are O(n), and the extra Space Complexities for `add`, `remove` are O(1). Since the print method is doing string concatenation is O(n), which can be O(1).

## How to setup
1. Install [Bundler](https://bundler.io/)
2. Install [IRB](https://rubygems.org/gems/irb)
3. Run `bundle install` in the root directory of this project



## How to Run

1. Launch IRB in the project directory by running  `irb -I .` in your console.
2. Use the RangeList in IRB console

```ruby
require 'range_list'
rl = RangeList.new
rl.add [1,5]
rl.print # "[1, 5)"
```



### Awailable Methods

#### Add

The new added range will be merged with the existing items if they are overlapped.

```ruby
rl.add [1,5]
rl.add([3, 8])
rl.print # "[1, 8)"
```



#### Remove

The existing items will be splitted or chompped depends on how many of the item is overlapped with the target range.

```ruby
rl.print # "[1, 8)"
rl.remove([2,4])
rl.print # "[1, 2) [4, 8)"
rl.remove([7,9])
rl.print # "[1, 2) [4, 7)"
```



#### Print

Printing the concated range list items.

```ruby
rl.print # "[1, 5)"
```



## How to test
1. Run `bundle exec rspec` after you get bundle installed

