module ListRangeable
  def insert_at_sorted_position(target)
    return if target[0] > target[1]
    linked_list.head = target and return if head.nil?

    prev = Node.new
    prev.next = head
    new_head = prev
    pos = head

    until pos.nil?
      if pos.overlap?(target) || target[1] < pos.value[0]
        linked_list.insert_around(prev, pos, target)
        break
      end

      if pos.next.nil?
        pos.next = target
        break
      end

      prev = pos
      pos = pos.next
    end

    linked_list.head = new_head.next
  end

  def merge_overlapped_nodes
    prev = head
    pos = head.next

    until pos.nil?
      if pos.overlap?(prev)
        prev.value = [[prev.value[0], pos.value[0]].min, [prev.value[1], pos.value[1]].max]
        prev.next = pos.next
      else
        prev = pos
      end

      pos = pos.next
    end
  end

  def cascading_remove(target)
    return if target[0] > target[1]

    prev = Node.new
    prev.next = head
    new_head = prev
    pos = head

    until pos.nil?
      if pos.overlap?(target)

        if target.cover?(pos)
          # delete this node
          linked_list.delete_node(prev, pos)
          pos = prev
          next
        elsif pos.full_include?(target)
          # split this node
          linked_list.split_node(prev, pos, target)
          break
        else
          pos.chomp!(target)
        end
      end

      prev = pos
      pos = pos.next
    end

    linked_list.head = new_head.next
  end
end
