module NodeComparable
  def full_include?(node)
    value[0] < node[0] && value[1] > node[1]
  end

  def cover?(node)
    (value[0]..value[1]).cover?((node[0]..node[1]))
  end

  def ==(other)
    value[0] == other[0] && value[1] == other[1]
  end

  def overlap?(node)
    node[0] <= value[1] && value[0] <= node[1]
  end

  def chomp!(node)
    @value = if node[0] > value[0]
               [value[0], node[0]]
             else
               [node[1] + 1, value[1]]
             end
  end
end
