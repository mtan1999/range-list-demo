class LinkedList
  attr_accessor :head

  def initialize
    @head = nil
  end

  def print
    res = []

    pos = head
    until pos.nil?
      res << pos
      pos = pos.next
    end

    res.join(' ')
  end

  def insert_around(prev_node, next_node, target)
    if next_node.value[1] < target[1]
      prev_node = next_node
      next_node = next_node.next
    end

    prev_node.next = target
    target.next = next_node
  end

  def delete_node(prev, current)
    prev.next = current.next
  end

  def split_node(prev, pos, range)
    first = Node.new([pos[0], range[0]])
    second = Node.new([range[1] + 1, pos[1]])

    prev.next = first
    first.next = second
    second.next = pos.next

    prev
  end
end
