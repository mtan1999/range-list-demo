class Node
  attr_accessor :next, :value

  extend Forwardable
  include NodeComparable

  def_delegator :@value, :[]

  def initialize(value = nil)
    @value = value
    @next  = nil
  end

  def to_s
    "[#{value.first}, #{value.last})"
  end
end
