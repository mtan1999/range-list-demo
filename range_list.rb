# frozen_string_literal: true

require_relative 'concerns/node_comparable'
require_relative 'concerns/list_rangeable'
require_relative 'node'
require_relative 'linked_list'
require 'forwardable'

class RangeList
  attr_reader :linked_list

  extend Forwardable
  include ListRangeable
  def_delegators :@linked_list, :print, :head

  def initialize
    @linked_list = LinkedList.new
  end

  # This method provides the ability of adding new range after compared with 
  # the existing items in Linked List.
  #
  # @example
  # rl = RangeList.new
  # rl.add([1, 5])
  # rl.print
  # # Should display: [1, 5)
  #   rl.add([10, 20])
  # rl.print
  # # Should display: [1, 5) [10, 20)
  # rl.add([20, 20])
  # rl.print
  # # Should display: [1, 5) [10, 20)
  def add(range)
    target = Node.new([range[0], range[1]])
    insert_at_sorted_position(target)
    merge_overlapped_nodes

    nil
  end

  # This method provides the ability of remove a range in Linked List.
  # There are three scenarios you may encounter.
  # 1. Not overlapped, it does nothing.
  # 2. Target range includes the existing items, it removes the existing items.
  # 3. Partial overlapped, it chomps the existing items.
  #
  # @examples
  # rl.print
  # # Should display: [1, 8) [10, 21)
  # rl.remove([10, 10])
  # rl.print
  # # Should display: [1, 8) [10, 21)
  def remove(range)
    target = Node.new([range[0], range[1] - 1])
    cascading_remove(target)

    nil
  end
end
