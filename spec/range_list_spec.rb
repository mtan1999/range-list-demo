require_relative '../range_list'

RSpec.describe RangeList do
  before { @range_list = RangeList.new }
  let(:default_ranges) { [[1, 5], [10, 21]] }

  describe '#add' do
    context 'when the range list is empty' do
      it 'works' do
        @range_list.add([1, 5])
        expect(@range_list.print).to eq('[1, 5)')

        @range_list.add([10, 20])
        expect(@range_list.print).to eq('[1, 5) [10, 20)')

        @range_list.add([20, 20])
        expect(@range_list.print).to eq('[1, 5) [10, 20)')
      end

      it 'sorts items' do
        @range_list.add([10, 20])
        expect(@range_list.print).to eq('[10, 20)')
        @range_list.add([1, 5])
        expect(@range_list.print).to eq('[1, 5) [10, 20)')
      end
    end

    context 'when the range list is not empty' do
      before :each do
        default_ranges.each do |v|
          @range_list.add v
        end
      end

      context 'within the existing number range' do
        let(:target) { [2, 4] }
        it 'changes nothing' do
          @range_list.add(target)
          expect(@range_list.print).to eq('[1, 5) [10, 21)')
        end
      end

      context 'when only overlapped with the first item' do
        let(:target) { [3, 8] }
        it 'extends the first item' do
          @range_list.add(target)
          expect(@range_list.print).to eq('[1, 8) [10, 21)')
        end
      end
    end
  end

  describe '#remove' do
    context 'when the range list is not empty' do
      before :each do
        default_ranges.each do |v|
          @range_list.add v
        end
      end

      context 'with an zero-range target' do
        let(:target) { [10, 10] }
        it 'does nothing' do
          @range_list.remove target
          expect(@range_list.print).to eq('[1, 5) [10, 21)')
        end
      end

      context 'when the target only overlaps left' do
        let(:target) { [10, 11] }
        it 'chomps the matched item' do
          @range_list.remove target
          expect(@range_list.print).to eq('[1, 5) [11, 21)')
        end
      end

      context 'when the target overlaps in the middle' do
        let(:target) { [15, 17] }
        it 'splits the overlapped item' do
          @range_list.remove target
          expect(@range_list.print).to eq('[1, 5) [10, 15) [17, 21)')
        end
      end

      context 'when the target overlaps two items' do
        let(:target) { [3, 19] }
        it 'chomps two items' do
          @range_list.remove target
          expect(@range_list.print).to eq('[1, 3) [19, 21)')
        end
      end
    end
  end
end
